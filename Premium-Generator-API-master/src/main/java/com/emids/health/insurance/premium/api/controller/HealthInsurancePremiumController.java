package com.emids.health.insurance.premium.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.emids.health.insurance.premium.api.common.Constants;
import com.emids.health.insurance.premium.api.dto.PremiumForm;
import com.emids.health.insurance.premium.api.dto.PremiumResponse;
import com.emids.health.insurance.premium.api.service.CalculatePremiumService;

@Controller
public class HealthInsurancePremiumController {
	
	@Autowired
	private CalculatePremiumService service;

	@RequestMapping(value = "/calculatePremium")
	public String calculateTotalPremium(
			@ModelAttribute("premiumForm") PremiumForm premiumForm, Model model) {
		double totalPremiumAmount = 0;
		PremiumResponse response = new PremiumResponse();
		try {
			totalPremiumAmount = service.getPremiumAmount(premiumForm);
			response.setAmount(totalPremiumAmount);
			response.setName(premiumForm.getName());
		} catch (Exception e) {
			model.addAttribute("errorMessage",
					Constants.HEALTH_INSURANCE_ERROR_MESSAGE);
			return "findPremium";
		}
		model.addAttribute("response", response);
		return "success";
	}

}
